FROM maven:3.8.6-openjdk-18 as builder
COPY ./ ./
RUN mvn package -DskipTests=true

FROM openjdk:18-jdk
RUN mkdir -p /target
COPY --from=builder target/backend-0.0.1-SNAPSHOT.jar /target/backend-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/target/backend-0.0.1-SNAPSHOT.jar"]
