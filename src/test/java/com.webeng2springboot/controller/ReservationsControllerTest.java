package com.webeng2springboot.controller;

import com.webeng2springboot.entity.ReservationDao;
import com.webeng2springboot.service.ReservationService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class ReservationsControllerTest {

    @Mock
    private ReservationService mockedReservationService;

    @Mock
    private ReservationDao mockedReservationDao;

    @Test
    void getReservationById() {
        UUID uuid = new UUID(123,456);

        Mockito.when(mockedReservationService.getReservationByID(uuid)).thenReturn(mockedReservationDao);
        ReservationDao bd = mockedReservationService.getReservationByID(uuid);
        assertNotNull(bd);
    }
}