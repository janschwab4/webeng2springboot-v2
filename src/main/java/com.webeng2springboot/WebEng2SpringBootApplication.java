package com.webeng2springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.SpringVersion;

@SpringBootApplication
public class WebEng2SpringBootApplication {

	public static void main(String[] args)
	{
		String a = SpringVersion.getVersion();

		SpringApplication.run(WebEng2SpringBootApplication.class, args);
	}
}
