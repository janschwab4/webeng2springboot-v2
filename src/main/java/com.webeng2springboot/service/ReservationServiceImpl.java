package com.webeng2springboot.service;

import com.webeng2springboot.entity.ReservationDao;
import com.webeng2springboot.interceptor.RoomChecker;
import com.webeng2springboot.repository.ReservationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Date;
import java.util.*;

@Service
public class ReservationServiceImpl implements ReservationService {

    @Autowired
    ReservationRepo reservationRepo;

    static final Logger logger = LoggerFactory.getLogger(ReservationServiceImpl.class);

    @Override
    public Map<String, List<String>> getStatus() {
        //TODO

        Map<String,List<String>> status = new HashMap<>();
        //List<List<String>> status = new ArrayList<>();
        List<String> authors = new ArrayList<>();
        List<String> apis = new ArrayList<>();
        List<String> apiVersion = new ArrayList<>();
        apiVersion.add("1");

        apis.add("status");
        apis.add("reservations-v1");

        authors.add("Jan Schwab");
        authors.add("Jannick Gutekunst");
        authors.add("Michael Blommer");

        status.put("authors", authors);
        status.put("supportedApis", apis);
        status.put("apiVersion", apiVersion);

        return status;
    }

    @Override
    public List<ReservationDao> getAllReservations(UUID room_id, Date before, Date after) {
        List<ReservationDao> list = reservationRepo.findAll();

        if (room_id != null) {
            // only give matching room ids
            list = (list.stream()).filter(reservation -> reservation.getRoom_id().equals(room_id)).toList();
        }
        if (before != null) {
            list = (list.stream()).filter(reservation -> reservation.getFromdate().compareTo(before) <= 0).toList();
        }
        if (after != null) {
            list = (list.stream()).filter(reservation -> reservation.getTodate().compareTo(after) >= 0).toList();
        }

        if (!list.isEmpty()) {
            logger.debug("Return reservations which match the given conditions. ");
            return list;
        }

        throw new NoSuchElementException("No reservations match conditions.");
    }

    @Override
    public ReservationDao addNewReservation(Date from, Date to, UUID room_id) throws IllegalAccessException{

        if(isRoomAvailable(room_id, from, to, null)) {
            // save
            try {
                return reservationRepo.save(new ReservationDao(from, to, room_id));
            } catch (Exception ex) {
                throw new IllegalAccessException(ex.getMessage() + "\nCould not save reservation to database.");
            }
        }
        logger.debug("Overlapping reservations with the following parameters: Room_id: " + room_id + " from: " + from + "to: " + to);
        throw new IllegalCallerException("Overlapping reservations.");
    }

    @Override
    public ReservationDao getReservationByID(UUID id) {
        logger.debug("Get Reservation with UUID" + id);
        return reservationRepo.findById(id).orElseThrow();
    }

    @Override
    public ReservationDao updateReservation(UUID id, Date from, Date to, UUID room_id) throws IllegalAccessException{

        logger.info("Try updating reservation: " + id.toString());
        ReservationDao reservations;
        try{
            reservations = reservationRepo.findById(id).orElseThrow();
        }catch (NoSuchElementException e){
            // create new reservation
            logger.info("Reservation does not exist yet. Creating new reservation.");
            reservations = new ReservationDao();
            reservations.setId(id);
        }

        if(isRoomAvailable(room_id, from, to, id)) {
            // save
            //TODO logger
            logger.debug("Try to Update Reservation with the following parameters : id: " + id + " from: " + from + "to: " + to + " room_id: " + room_id);
            try {
                reservations.setFromdate(from);
                reservations.setTodate(to);
                reservations.setRoom_id(room_id);
                return reservationRepo.save(reservations);
            } catch (Exception ex) {
                throw new IllegalAccessException(ex.getMessage() + "\nCould not save reservation to database.");
            }
        }

        throw new IllegalAccessException("\nCould not save reservation to database.");
    }

    @Override
    public Boolean deleteReservation(UUID id) {
        logger.debug("Try to delete Reservation with id: " + id);
        reservationRepo.findById(id).orElseThrow();
        reservationRepo.deleteById(id);
        return true;
    }

    private boolean isRoomAvailable(UUID room_id, Date from, Date to, UUID reservationToUpdate) {
        // check if room exists
        RoomChecker roomChecker = new RoomChecker();
        if (room_id == null || !roomChecker.checkIfRoomExists(room_id.toString())) {
            logger.debug("No room with room_id " + room_id + " exists.");
            throw new NoSuchElementException("No room exists to given roomId.");
        }

        // check if dates make sense
        if (from.compareTo(to) >= 0){ // to > from
            logger.debug("from date " + from + " must be before to date " + to);
            throw  new IllegalArgumentException("From must be less than to.");
        }

        // check for conflicts
        // TODO
        List<ReservationDao> possibleConflicts =
                (reservationRepo.findAll().stream()).filter(reservation -> reservation.getRoom_id().equals(room_id)).toList();

        for (ReservationDao r : possibleConflicts) {
            if (r.getRoom_id().equals(room_id) && !r.getId().equals(reservationToUpdate)) {
                // it is same room -> check for same dates
                if (from.toLocalDate().compareTo(r.getFromdate().toLocalDate()) > 0 // from > r.getFrom()
                        && from.toLocalDate().compareTo(r.getTodate().toLocalDate()) < 0) // from < r.getTo()
                {
                    // from-date is within existing reservation
                    throw new IllegalCallerException("Overlapping reservations.");
                }

                if (to.toLocalDate().compareTo(r.getFromdate().toLocalDate()) > 0 // to > r.getFrom()
                        && to.toLocalDate().compareTo(r.getTodate().toLocalDate()) < 0) // to < r.getTo()
                {
                    // to-date is within existing reservation
                    throw new IllegalCallerException("Overlapping reservations.");
                }

                if (from.toLocalDate().compareTo(r.getFromdate().toLocalDate()) <= 0 // from < r.getFrom()
                        && to.toLocalDate().compareTo(r.getTodate().toLocalDate()) >= 0) // to > r.getTo()
                {
                    // dates of new reservation are surrounding an existing reservation
                    throw new IllegalCallerException("Overlapping reservations.");
                }
            }
        }

        return true;
    }

}


