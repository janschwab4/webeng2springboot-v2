package com.webeng2springboot.service;

import com.webeng2springboot.entity.ReservationDao;

import java.sql.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public interface ReservationService {

    Map<String, List<String>> getStatus();

    List<ReservationDao> getAllReservations(UUID room_id, Date before, Date after);

    ReservationDao addNewReservation(Date from, Date to, UUID room_id) throws IllegalAccessException;

    ReservationDao getReservationByID(UUID uuid);

    ReservationDao updateReservation(UUID id, Date from, Date to, UUID room_id) throws  IllegalAccessException;

    Boolean deleteReservation(UUID uuid) /*TODO: throws Exception*/;

}
