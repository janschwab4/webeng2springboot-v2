package com.webeng2springboot.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

//import javax.persistence.*;
import java.sql.Date;
import java.util.UUID;


@Data
@NoArgsConstructor
@Entity
@EnableAutoConfiguration
@Table(name="reservations")
public class ReservationDao {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    private Date fromdate;
    private Date todate;
    private UUID room_id;

    public ReservationDao(Date fromdate, Date todate, UUID room_id){
        this.fromdate = fromdate;
        this.todate = todate;
        this.room_id = room_id;
    }


    public ReservationDto toDto() {
        return new ReservationDto(fromdate, todate, room_id);
    }

}
