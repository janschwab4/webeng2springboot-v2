package com.webeng2springboot.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
public class ReservationDto {

    private Date from;
    private Date to;
    private UUID room_id;

}
