package com.webeng2springboot.interceptor;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

import java.util.Objects;

import org.apache.commons.codec.binary.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Component
public class Authenticator implements HandlerInterceptor {

    private JSONObject pubKeyJSON;
    private HttpURLConnection conn;

    static final Logger logger = LoggerFactory.getLogger(Authenticator.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        if (Objects.equals(request.getMethod(), "GET"))
        {
            return true;
        }
        pubKeyJSON = getPubKeyJSON();
        if (!isValid(request.getHeader("Authorization"))) {
            response.setStatus(401);
            logger.warn("Authentication failed. Token not valid");

            response.setContentType("application/json");
            try{
                response.getWriter().write("{\"message\": \"Authorization failed.\"}");
            }catch (Exception e){
                logger.error("Error creating error message in body.");
            }

            return false;
        }
        return true;
    }

    public JSONObject getPubKeyJSON() {
        BufferedReader reader;
        String line;
        StringBuilder responseContent = new StringBuilder();
        String response = "";
        try{
            URL url = new URL(System.getenv("KEYCLOAK"));
            //URL url = new URL("http://keycloak:8080/auth/realms/biletado");
            //URL url = new URL("http://localhost/auth/realms/biletado");    //TODO
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(3000);
            conn.setReadTimeout(3000);

            int status = conn.getResponseCode();

            if (status >= 300) {
                reader = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
            }
            else {
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            }
            while ((line = reader.readLine()) != null) {
                responseContent.append(line);
            }
            reader.close();
            response = responseContent.toString();
        } catch (MalformedURLException e) {
            logger.error("MalformedURLException: " + e.getMessage());
        } catch (IOException e) {
            logger.error("IOException: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        return new JSONObject(response);
    }

    public RSAPublicKey getPubKeyObject(String pubKeyString) {
        byte[] encoded = Base64.decodeBase64(pubKeyString);
        try {
            KeyFactory kf = KeyFactory.getInstance("RSA");
            return (RSAPublicKey) kf.generatePublic(new X509EncodedKeySpec(encoded));
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            logger.error("Public key convert failed: " + e.getMessage());
        }
        return null;
    }

    public boolean isValid(String authHeader) {
        String pubKeyString = pubKeyJSON.get("public_key").toString();

        try {
            String[] token = authHeader.split(" ");
            RSAPublicKey publicKey = getPubKeyObject(pubKeyString);
            Algorithm algorithm = Algorithm.RSA256(publicKey, null);
            JWTVerifier verifier = JWT.require(algorithm)
                    .build();
            verifier.verify(token[1]);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

}
