package com.webeng2springboot.interceptor;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Component
public class Tracer implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {

        String traceHeader = request.getHeader("uber-trace-id");
        if (traceHeader != null) {
            response.setHeader("uber-trace-id", traceHeader);
        }
        return true;

    }

}
