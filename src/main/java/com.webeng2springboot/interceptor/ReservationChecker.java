package com.webeng2springboot.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.util.Objects;

@Component
public class ReservationChecker implements HandlerInterceptor {

    HttpURLConnection conn;

    static final Logger logger = LoggerFactory.getLogger(ReservationChecker.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        String requestUrl = request.getRequestURL().toString();
        if (Objects.equals(request.getMethod(), "DELETE") && requestUrl.contains("/assets/rooms/"))
        {
            String[] url = requestUrl.split("/");
            if (checkForReservation(url[5])) {
                logger.warn("There are reservations for this room");
                response.setStatus(451);
                return false;
            }
        }
        return true;
    }

    private String getReservations() {

        BufferedReader reader;
        String line;
        StringBuilder responseContent = new StringBuilder();
        String response = "";
        try {
            URL url = new URL("http://backend-reservations/api/reservations/");
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(3000);
            conn.setReadTimeout(3000);

            int status = conn.getResponseCode();

            if (status >= 300) {
                reader = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
            } else {
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            }
            while ((line = reader.readLine()) != null) {
                responseContent.append(line);
            }
            reader.close();
            response = responseContent.toString();
        } catch (MalformedURLException e) {
            logger.error("MalformedURLException: " + e.getMessage());
        } catch (IOException e) {
            logger.error("IOException: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        return response;
    }

    public boolean checkForReservation(String uuid) {
        return getReservations().contains(uuid);
    }

}
