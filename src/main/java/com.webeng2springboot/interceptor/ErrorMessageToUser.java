package com.webeng2springboot.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;

public class ErrorMessageToUser implements HandlerInterceptor{


    @Override
    public boolean preHandle(jakarta.servlet.http.HttpServletRequest request, jakarta.servlet.http.HttpServletResponse response, java.lang.Object handler) throws java.lang.Exception {
        return true;
    }

    @Override
    public void postHandle(jakarta.servlet.http.HttpServletRequest request, jakarta.servlet.http.HttpServletResponse response, java.lang.Object handler, @org.springframework.lang.Nullable org.springframework.web.servlet.ModelAndView modelAndView) throws java.lang.Exception {
       if(response.getStatus() >= 300){
           String msg = response.getHeader("ErrorMessage");
           response.getWriter().write("{\"message\": \"" + msg + "\"}");
           response.setContentType("application/json");
       }
    }

    @Override
    public void afterCompletion(jakarta.servlet.http.HttpServletRequest request, jakarta.servlet.http.HttpServletResponse response, java.lang.Object handler, @org.springframework.lang.Nullable java.lang.Exception ex) throws java.lang.Exception {
        return;
    }


}
