package com.webeng2springboot.interceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new Tracer());
        registry.addInterceptor(new Authenticator());
        registry.addInterceptor(new ReservationChecker());
        registry.addInterceptor(new ErrorMessageToUser());
    }

}
