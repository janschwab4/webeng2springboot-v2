package com.webeng2springboot.repository;

import com.webeng2springboot.entity.ReservationDao;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ReservationRepo extends JpaRepository<ReservationDao, UUID> {




}
