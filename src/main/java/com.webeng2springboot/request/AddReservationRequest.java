package com.webeng2springboot.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Date;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AddReservationRequest {
    UUID id;
    Date from;
    Date to;
    UUID room_id;

    public AddReservationRequest(Date from, Date to, UUID room_id){
        this.from = from;
        this.to = to;
        this.room_id = room_id;
    }

}
