package com.webeng2springboot.controller;

import com.webeng2springboot.entity.ReservationDao;
import com.webeng2springboot.request.AddReservationRequest;
import com.webeng2springboot.service.ReservationService;
import com.webeng2springboot.request.UpdateReservationRequest;
import com.webeng2springboot.utils.HeaderCreator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.*;

@RestController
@RequestMapping("/reservations/")
public class ReservationsController {

    @Autowired
    ReservationService reservationService;

    static final Logger logger = LoggerFactory.getLogger(ReservationsController.class);

    @GetMapping()
    public ResponseEntity<List<ReservationDao>> getAllReservations(@RequestParam(required = false) UUID room_id,
                                                                   @RequestParam(required = false) Date before,
                                                                   @RequestParam(required = false) Date after){
        try {
            System.out.println("Try getting all reservations...");
            logger.info("Try getting all reservations...");
            return new ResponseEntity<>(reservationService.getAllReservations(room_id, before, after), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            logger.warn("No reservation found: " + e.getMessage());
            return HeaderCreator.CreateErrorMsgHeader(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            System.out.println("Server Error");
            logger.error("Server Error: " + e.getMessage());
            return HeaderCreator.CreateHeaderInternalServerError(e);
        }
    }
    @GetMapping("/status/")
    public ResponseEntity<Map<String, List<String>>> getReservationStatus(){
        try {
            logger.info("Try getting status...");
            return new ResponseEntity<>(reservationService.getStatus(), null , HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Server Error: " + e.getMessage());
            return HeaderCreator.CreateHeaderInternalServerError(e);
        }
    }
    @PostMapping()
    public ResponseEntity<ReservationDao> addNewReservation(@RequestBody AddReservationRequest addReservationRequest)
    {
        try {
            if(addReservationRequest.getId() != null){
            // reservation already exists
            // update reservation
            logger.info("Try updating reservation...");
            return new ResponseEntity<>(reservationService.updateReservation(
                    addReservationRequest.getId(),
                    addReservationRequest.getFrom(),
                    addReservationRequest.getTo(),
                    addReservationRequest.getRoom_id()),
                    HttpStatus.CREATED);
        }else{
            // build new reservation
            System.out.println("Try adding new reservation...");
            logger.info("Try adding new reservation...");
            return new ResponseEntity<>(reservationService.addNewReservation(
                    addReservationRequest.getFrom(),
                    addReservationRequest.getTo(),
                    addReservationRequest.getRoom_id()),
                    HttpStatus.CREATED);
        }
        //}catch () {
            // authentication required (401) -> handled by interceptor
        }catch(NoSuchElementException e) {
            // room does not exist: 422
            logger.warn("Room does not exist: " + e.getMessage());
            return HeaderCreator.CreateErrorMsgHeader(e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
        } catch (IllegalCallerException e) {
            // conflict with other reservation in same room (409)
            logger.warn("Overlapping reservations: " + e.getMessage());
            return HeaderCreator.CreateErrorMsgHeader(e.getMessage(), HttpStatus.CONFLICT);
        } catch (IllegalAccessException e) {
            // problem with database access
            logger.error("Problem with database access: " + e.getMessage());
            return HeaderCreator.CreateErrorMsgHeader(e.getMessage(), HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);
        }catch (IllegalArgumentException e){
            logger.error("Illegal Argument: " + e.getMessage());
            return HeaderCreator.CreateErrorMsgHeader(e.getMessage(), HttpStatus.CONFLICT);
        } catch (Exception e) {
            logger.error("Server Error: " + e.getMessage());
            return HeaderCreator.CreateHeaderInternalServerError(e);
        }
    }

    @GetMapping("/{id}/")
    public ResponseEntity<ReservationDao> getReservationById(@PathVariable UUID id){
        try {
            logger.info("Try getting reservation...");
            return new ResponseEntity<>(reservationService.getReservationByID(id), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            logger.warn("Reservation not found: " + e.getMessage());
            return HeaderCreator.CreateErrorMsgHeader(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Server Error:" + e.getMessage());
            return HeaderCreator.CreateHeaderInternalServerError(e);
        }
    }

    @PutMapping("/{id}/")
    public ResponseEntity<ReservationDao> updateReservation(@PathVariable UUID id,
                                                            @RequestBody UpdateReservationRequest updateReservationRequest){
        // bodyId is optional
        UUID bodyId = updateReservationRequest.getId();
        if (bodyId != null && !bodyId.equals(id)) {
            logger.error("ReservationId " + id + " and Body " + bodyId + "not equal");
            //error code: 422
            return HeaderCreator.CreateErrorMsgHeader("ReservationId in URL and Body not equal",
                    HttpStatus.UNPROCESSABLE_ENTITY);
        }
        try {
            logger.info("Try updating reservation...");
            return new ResponseEntity<>(reservationService.updateReservation(
                    id,
                    updateReservationRequest.getFrom(),
                    updateReservationRequest.getTo(),
                    updateReservationRequest.getRoom_id()),
                    HttpStatus.NO_CONTENT);
        } catch (NoSuchElementException e) {
            //error code: 422
            logger.warn("Reservation not found: " + e.getMessage());
            return HeaderCreator.CreateErrorMsgHeader(e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
        }catch (IllegalCallerException e) {
            // conflict with other reservation in same room (422)
            logger.warn("Overlapping reservations: " + e.getMessage());
            return HeaderCreator.CreateErrorMsgHeader(e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
        }catch(IllegalAccessException e){
            logger.error("Could not save reservation to database: " + e.getMessage());
            return HeaderCreator.CreateErrorMsgHeader(e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
        } catch (Exception e) {
            logger.error("Server Error: " + e.getMessage());
            return HeaderCreator.CreateHeaderInternalServerError(e);
        }
    }

    @DeleteMapping("/{id}/")
    public ResponseEntity<Boolean> deleteReservation(@PathVariable UUID id) {
        try {
            logger.info("Try deleting reservation...");
            return new ResponseEntity<>(reservationService.deleteReservation(id), HttpStatus.NO_CONTENT);
        } catch (NoSuchElementException e) {
            logger.warn("Reservation not found: " + e.getMessage());
            return HeaderCreator.CreateErrorMsgHeader(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("Server Error: " + e.getMessage());
            return HeaderCreator.CreateHeaderInternalServerError(e);
        }
    }
}
