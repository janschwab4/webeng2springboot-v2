package com.webeng2springboot.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.ArrayList;
import java.util.List;

public class HeaderCreator {

    public static ResponseEntity CreateErrorMsgHeader(String errMsg, HttpStatus status){
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();

        List<String> headerValues = new ArrayList<>();
        headerValues.add(errMsg);
        headers.put("ErrorMessage", headerValues);
        return new ResponseEntity<>(headers, status);
    }

    public static ResponseEntity CreateHeaderInternalServerError(Exception e){
        return CreateErrorMsgHeader("Internal server error.\n" + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }


}
