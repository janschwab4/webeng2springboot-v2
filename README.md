This backend application is written in Java Spring Boot.

With the enviroment variable *LOG_LEVEL* can the log level be changed. Default is INFO (needs to be checked if it still is). And you will also get all the messages for the levels above. For Example, with LOG LEVEL Info, you will get all the error, warn and info messages.
Options are:
+ ERROR
+ WARN
+ INFO
+ DEBUG

URL Umgebungsvariablen:
+ TRAEFIK_ROOMS: http://traefik/api/assets/rooms/
+ BACKEND_RESERVATIONS: http://backend-reservations/api/reservations/
+ KEYCLOAK: http://keycloak:8080/auth/realms/biletado